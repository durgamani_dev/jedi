package com.assignement.problems.week3;


public class CircularLinkedList {
	static class Node{
		String data;
		Node next;
	}
	
	static boolean isCircular(Node head) {
		if(head == null)return false;
		
		Node node = head.next;
	
		while(node!=null&& node!=head) {
			node = node.next;
		}
		return (node==head);
		
		
	}
		
	static Node newNode(String data) {
		Node temp = new Node();
		temp.data = data;
		temp.next = null;
		return temp;
	}
		
		
	

	public static void main(String args[]) {
		
		Node head = newNode("a");
		Node n2 = newNode("b");
		Node n3 = newNode("r");
		Node n4 = newNode("c");
		head.next = n2;
		n2.next = n3;
		n3.next = n4;
		
		System.out.println(isCircular(head)?"list is circular" : "list is  not circular");
		
		n4.next = head;
		
		System.out.println(isCircular(n2)?"list is circular" : "list is  not circular");
		
	}
}