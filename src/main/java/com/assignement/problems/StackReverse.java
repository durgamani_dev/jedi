package com.assignement.problems;
import java.util.Stack;

public class StackReverse {

	static Stack<Integer> stack = new Stack<Integer>();
	public static void insert(int x) {
		if(stack.isEmpty())
			stack.push(x);
		else {
			int t = stack.peek();
			stack.pop();
			insert(x);
			stack.push(t);
			
		}
		
	}
	
	public static  void reverse() {
		if(stack.size()>0) {
			int x = stack.peek();
			stack.pop();
			reverse();
			insert(x);
		}
	}
	
	public static void main(String args[]) {
		 
		stack.push(1);
		stack.push(2);
		stack.push(3);
		stack.push(4);
		
		System.out.print(stack);
	   reverse();
	   System.out.print(stack);
	}
}
