package com.assignement.problems;


public class DoubleLinkedList{

	static class Node{
		String data;
		Node next,prev;
		
		public Node(String data,Node prev,Node next) {
			this.data = data;
			this.next = next;
			this.prev = prev;
		}
		
	}
	
	static Node deleteNode(Node root,int i) {
		Node del = nodeAtGivenPos(root,i);
		if(root == null || del == null)
			return null;
		if(root == del)
			root = del.next;
		if(del.next != null)
			del.next.prev = del.next;
		if(del.prev !=null)
			del.prev.next = del.next;
			
		del = null;
		
		return root;
		
	}
	static Node insertNode(Node root,int i,String value) {
		Node newNode = new Node(value,null,null);
		
		if(i<1) 
			System.out.print("invalid position");
	
		Node current = nodeAtGivenPos(root,i);
		if(root == null || current == null)
			return null;
	
		if(root == current)
			newNode.prev = root;
		if(current.next != null) {
			newNode.next = current.next;
			(current.next).prev = newNode;
			current.next = newNode;
			newNode.prev= current;
			
		}
		
		return root; 
		
	}
	
	static Node nodeAtGivenPos(Node root, int n) {
		if(root == null || n<= 0)
			return null;
		Node temp = root;
	
		for(int i=1;temp !=null && i<n; i++) {
			temp = temp.next;
		}
		if ( temp == null)
			return null;
		
		
		return temp;
	}
	
	static Node insert(Node root, String data) {
		Node node = new Node(data,null,root);
		
		if( root != null)
			root.prev = node;
		root = node;
		return root;
	}
	
	static void printList(Node root) {
		if(root == null)
			System.out.print("Doubly Linked list is empty");
		while(root!= null)
		{
			System.out.print(root.data + " ");
			root = root.next;
		}
		
		System.out.println();
	}
		
	 public static void main(String[] args) 
	    { 
	        
	        Node root = null; 
	  
	        root = insert(root, "2"); 
	        root = insert(root, "5"); 
	        root = insert(root, "4"); 
	        root = insert(root, "8"); 
	        root = insert(root, "10"); 
	  
	        System.out.println("Doubly linked list before deletion:"); 
	        printList(root); 
	        int n = 2; 
	        deleteNode(root, n); 
	        System.out.println("Doubly linked list after deletion:"); 
	        printList(root); 
	        
	        root = insertNode(root, 3, "99");  
	        System.out.print("Double linked list after insertion");
	        printList(root);
	    } 
}
