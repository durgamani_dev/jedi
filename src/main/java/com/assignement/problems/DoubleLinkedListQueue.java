package com.assignement.problems;

public class DoubleLinkedListQueue {

	 Node head;
	 Node tail;
	 
	 static class Node{
		 int i;
		 Node next;
		 Node prev;
		 public Node(int i) {
			 this.i  = i;
			 this.prev = null;
			 this.next = null;
			 
		 }
	 }
		 
		 public boolean isEmpty() {
			 return head == null;
		 }
		 public void insertFirst(int i) {
			 Node newNode = new Node(i);
			 if(isEmpty()) {
				 tail = newNode;
			 }else {
				 head.prev = newNode;
			 }
			 newNode.next = head;
			 head = newNode;
		 }
		 
		 public void insertLast(int i) {
			 Node newNode = new Node(i);
			 if(isEmpty()) {
				 head = newNode;
			 }else {
				 tail.next = newNode;
				 newNode.prev = tail;
			 }
				tail = newNode; 
		 }
		 
		 public Node removeFirst() {
			 if(isEmpty())
				 System.out.print("queue is empty");
			 Node first = head;
			 if(head.next == null)
				 tail = null;
			 else {
				 head.next.prev = null;
			 }
				 head = head.next;
				 return first;
		 }
		 
		 public Node removeLast() {
			 if(isEmpty())
				 System.out.print("queue is empty");
			 Node last = tail;
			 if(head.next == null)
				 head = null;
			 else{
				 tail.prev.next = null;
			 }
			 tail = tail.prev;
			 return last;
		 }
		 
		 public void displayFromFirst() {
			 Node current = head;
			 while(current != null) {
				 System.out.print(current.i);
				 current = current.next;
			 }
			 System.out.println(" ");
		 }
		 
		 public void displayFromLast() {
			 Node current = tail;
			
			 while(current !=null) {
				 System.out.print(current.i);
				 current = current.prev;
			 }
			 System.out.println(" ");
		 }
		 
		 public static void main(String[] args) {
			  DoubleLinkedListQueue queue = new DoubleLinkedListQueue(); 
			 
			  queue.insertFirst(2);  
			  queue.insertFirst(1);
			  queue.insertLast(3);
			  queue.insertLast(4);
			  System.out.print("displaying from first: ");
			  queue.displayFromFirst();
			  Node node = queue.removeFirst();
			  System.out.println("Node with value "+ node.i + " deleted");
			  System.out.print("displaying from Last: ");
			  queue.displayFromLast();
			  
			 }
	
}
