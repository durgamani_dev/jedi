package com.assignement.problems;

public class LinkedListMergeSort {
	
	static class Node{
		int data;
		Node next;
		
		public Node(int data) {
			this.data = data;
			this.next = null;
		}
	}
	
	public  static Node add(int value,Node root) {
		Node newNode = new Node(value);
		
		Node temp = root;
		if(root == null) {
			return newNode;
		}
		while(temp.next!=null)
			temp = temp.next;
		temp.next = newNode;
		
		return root;
	}

	public static void printList(Node root) {
		Node temp = root;
		while(temp!=null) {
			System.out.print(temp.data+" ");
			temp = temp.next;
		}
			
	}
	
	public static Node merge(Node list1,Node list2) {
		
		Node temp = new Node(0);
		Node tail = temp;
		while(true) {
			if(list1 == null) {
				tail.next = list2;
				break;
			}
			if(list2 == null) {
				tail.next = list1;
				break;
			}
			if(list1.data<=list2.data) {
				Node node = new Node(list1.data);
				tail.next = node;
				list1 = list1.next;
			}else {
				Node node = new Node(list2.data);
				tail.next = node;
				list2 = list2.next;
			}
			
			tail = tail.next;
		}
		
		return temp.next;
	}
	
	public static void main(String args[]) {
		Node llist1 = null; 
	    Node llist2 = null; 
	      
	    // Node head1 = new Node(5); 
	   llist1 = add(5,llist1); 
	   llist1 = add(10,llist1); 
	    llist1 = add(15,llist1); 
	    System.out.print("list1");
	    printList(llist1);
	     System.out.println(); 
	    // Node head2 = new Node(2); 
	    llist2 = add(2,llist2); 
	    llist2 = add(3,llist2); 
	    llist2 = add(20,llist2); 
	    System.out.print("list2");
	    printList(llist2);
	    System.out.println();
	    Node root = merge(llist1,llist2);
	    System.out.print("after merging");
	    printList(root);
	}
		
	
}
