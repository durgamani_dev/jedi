package com.assignement.problems;

public class DoubleLinkedListReversal {

	static class Node{
		String data;
		Node next,prev;
		
		public Node(String data,Node prev,Node next) {
			this.data = data;
			this.next = next;
			this.prev = prev;
		}
	}
	
	static Node insert(Node root, String data) {
		Node node = new Node(data,null,root);
		
		if( root != null)
			root.prev = node;
		root = node;
		return root;
	}
	
	static void printList(Node root) {
		if(root == null)
			System.out.print("Doubly Linked list is empty");
		while(root!= null)
		{
			System.out.print(root.data + " ");
			root = root.next;
		}
		
		System.out.println();
	}
	
	static Node reverse(Node root) {
		Node temp = null;
		Node current = root;
		while(current!=null) {
			temp = current.prev;
			current.prev = current.next;
			current.next = temp;
			current = current.prev;
		}
		if(temp!=null)
			root = temp.prev;
		
		return root;
	}
		
	 public static void main(String[] args) 
	    { 
	        
	        Node root = null; 
	  
	        root = insert(root, "2"); 
	        root = insert(root, "5"); 
	        root = insert(root, "4"); 
	        root = insert(root, "8"); 
	        root = insert(root, "10"); 
	        System.out.println("linked list before reversal");
	        printList(root);
	        root= reverse(root);
	        System.out.println("linked list after reversal");
	        printList(root);
	    }
}
