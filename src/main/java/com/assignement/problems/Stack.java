package com.assignement.problems;

public class Stack {

	static int top =-1;
	static final int MAX = 100;
	int[] a = new int[MAX];
	static boolean isEmpty() {
		return (top<0);
	}
	
	public void push(int x) {
		if(top>= MAX-1) {
			System.out.println("stack is full");
		}else {
			a[++top] = x;
			
		}
	}
	
	public int pop() {
		if(isEmpty()) {
			System.out.print("Satck is empty");
			return 0;
			
		}else {
			int x = a[top--];
			return x;
		}
	}
	
	public int peek() {
		if(isEmpty()) {
			System.out.print("Stack is empty");
			return 0;
		}else {
			int x = a[top];
			return x;
		}
	}
	
	public static void main(String args[]) {
 
	        Stack stack = new Stack(); 
	        stack.push(10); 
	        stack.push(20); 
	        stack.push(30); 
	        System.out.println(stack.pop() + " Popped from stack"); 
	    } 
}
