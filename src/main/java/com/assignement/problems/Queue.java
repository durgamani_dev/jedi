package com.assignement.problems;

public class Queue {
	private static int front=0,rear=0,capacity =100;
	private static int a[] = new int[capacity];
	
	static void enqueue(int item) {
		if(capacity == rear) {
			System.out.print("Queue is full");
		}else {
			a[rear] = item;
			rear++;
		}
	}
	static int dequeue() {
		if(front == rear) {
			System.out.print("Queue is empty");
			return 0;
		}
		else {
			int x = a[front];
			front++;
			return x;
		}
			
	}
	
	public static void main(String args[]) {
		enqueue(4);
		enqueue(6);
		enqueue(7);
		enqueue(9);
		enqueue(3);
		System.out.print(dequeue());
	}

}
