package com.assignement.problems;

import java.util.LinkedList;
import java.util.Queue;


public class QueueReversal {
	static Queue<Integer> queue;
	
    public static void reverse(){
	   if(!queue.isEmpty()) {
		int item = queue.peek();
		queue.remove();
	
		reverse();
		queue.add(item);
	  }
   }

   public static void main(String args[]) 
  { 
    queue = new LinkedList<Integer>(); 
    queue.add(1); 
    queue.add(2); 
    queue.add(3); 
    queue.add(4); 
    queue.add(5); 
    System.out.print("Queue:"+queue+" ");
     reverse(); 
    System.out.print("queue after reversal:" + queue); 
  } 
}
